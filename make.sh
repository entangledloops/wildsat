#!/bin/bash
DEBUG_MODE="FALSE"
if [ $DEBUG_MODE = "TRUE" ] 
then
  sudo pkill -9 -f "run" &> /dev/null
  clang++ src/*.cpp -U__STRICT_ANSI__ -stdlib=libc++ -std=c++0x -Wall -Wextra -pedantic -o wildsat
else
  g++ src/*.cpp -U__STRICT_ANSI__ -lstdc++ -std=c++0x -Wall -Wextra -pedantic -O3 -fno-exceptions -o wildsat
 fi
