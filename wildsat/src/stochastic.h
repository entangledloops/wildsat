//
//  stochastic.h
//  wildsat
//
//  Created by Stephen Dunn on 5/4/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#ifndef __wildsat__stochastic__
#define __wildsat__stochastic__

#include <iostream>
#include <vector>
#include <set>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "utils.h"
#include "hashtable.h"
#include "satsolver.h"

//#define DEBUG_MODE // debug output (print full node strings)
//#define VERBOSE_MODE // render node counts during search
#define PRINT_COUNT 1000
#define RAND_SEED 122288
#define FLOAT_INFINITY std::numeric_limits<float>::infinity()
#define NOW() (float)clock()/CLOCKS_PER_SEC
#define EVER ;;

//=================================
class Stochastic;

struct state_s {
  state_s() {}
  state_s(size_t lit, LIT_VALUE val) : lit(lit),val(val) {}
  
  size_t lit;
  LIT_VALUE val;
};

struct state_sorter {
  inline bool operator()(const state_s &l, const state_s &r) {
    return l.lit < r.lit;
  }
};

struct feature_s {
  feature_s(size_t nv) { v.reserve(nv); }
  std::vector<size_t> v;
  std::vector<pclause> c;
};
    
struct feature_sorter {
  inline bool operator()(const feature_s &l, const feature_s &r) {
    return l.v.size() < r.v.size();
  }
};

struct var_info_s {
  var_info_s() : used(false) { reset(); val = LIT_UNSET; ratioRegNeg = 0;}
  void reset() { ratioLitVar=reg=neg=hits=0; }
  LIT_VALUE bestVal() { return neg > reg ? LIT_FALSE : LIT_TRUE; }
  
  bool used;
  float ratioRegNeg, ratioLitVar;
  LIT_VALUE val;
  size_t reg, neg, hits;
};

//=================================

class Stochastic : public SatSolver {
  
public:
  
  class Node;
  typedef std::shared_ptr<Stochastic::Node> pnode;
  
  Stochastic(const size_t &nv, const size_t &nc, bool gui);
  ~Stochastic() {}
  
  virtual bool addClause(pclause c);
  virtual bool satisfiable();
  virtual std::string solution();
  virtual std::string stats();
  
  size_t h(pnode n);
  
  std::vector<size_t> getHits(size_t n, bool high) const;
  std::vector<size_t> mostHits(size_t n) const { return getHits(n,true); }
  std::vector<size_t> leastHits(size_t n) const { return getHits(n,false); }
  
  std::vector<state_s> getAtomic() const;
  std::vector<state_s> getPure();
  std::vector<state_s> getState() const;
  
  feature_s getDisjoint(feature_s seen, std::vector<size_t> nextVisit);
  std::vector<size_t> getDisjointVars(size_t startVar);
  std::vector<pclause> getDisjointClauses(size_t startVar);
  std::vector<feature_s> getDisjointGraphs();
  
  bool solve();
  bool goal(pnode n);
  bool addNode(pnode n);
  
  LIT_VALUE expand(pnode n);
  LIT_VALUE restart();
  
  void assign(size_t var, LIT_VALUE val);
  void assignAll();
  void update();
  
  // ==============================
  
  unsigned int seed;
  double expanded, generated, restartTime, toGenerate;
  
  pnode soln; // solution node, 1st node added (for cycle detection)
  size_t tableSize, printCount, hMax;
  float startTime, elapsedTime, lastTime, maxRestarts;
  
  std::vector<state_s> pure;
  std::vector<var_info_s> info;
  
  std::vector<std::vector<pclause>> links;
  std::vector<std::vector<pnode>> closed;
  std::vector<pnode> open;
  std::vector<pclause> c;

  // ================================
  
  class Node : public utils::Hashable,
  public std::enable_shared_from_this<Stochastic::Node> {
  public:
    
    Node(Stochastic *s, pnode parent=NULL, state_s *mod=NULL) {
      this->s = s; this->parent = parent; h = 0;
      if (parent) depth = parent->depth+1; else depth = 0;
      if (mod) state.push_back(*mod);
    }
    ~Node() {}
    
    pnode getThis() { return shared_from_this(); }
    
    void undo();
    void assign();
    void setH() { h = s->h( getThis() ); }
    void push(state_s s) { state.push_back(s); }
    void push(size_t var, LIT_VALUE val) { push(state_s( var, val )); }
    
    LIT_VALUE isSatisfied();
    
    std::vector<state_s> getState() const;
    std::string toString();
    std::string toGUIString() const;
    
    // ====================
    
    size_t h, depth;
    Stochastic *s;
    std::vector<state_s> state;
    pnode parent;
    
  };
  
  struct node_sorter {
    inline bool operator()(const pnode &l, const pnode &r) const {
      return l->h > r->h;
    }
  };
  
  struct node_rev_sorter {
    inline bool operator()(const pnode &l, const pnode &r) const {
      return l->h < r->h;
    }
  };
  
};



#endif /* defined(__wildsat__stochastic__) */
