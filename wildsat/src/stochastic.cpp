//
//  stochastic.cpp
//  wildsat
//
//  Created by Stephen Dunn on 5/4/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#include "stochastic.h"

using std::cout; using std::endl;

Stochastic::Stochastic(const size_t &nv, const size_t &nc, bool gui) :
SatSolver(nv,nc,gui)
{
#ifdef DEBUG_MODE
  seed = RAND_SEED;
#else
  seed = (unsigned int)time(NULL);
#endif
  srand( seed );
  
  soln = NULL;
  restarts = 0;
  hMax = nc*2;
  maxRestarts = FLOAT_INFINITY;
  restartTime = log2f( nv )*2;
  tableSize = nv * nc * 2;
  conflicts = decisions = expanded = generated = 0;
  toGenerate = ceilf( log2f(nv) )*2;
  
  if (toGenerate < 10) toGenerate = 10;
  if (toGenerate > nv) toGenerate = nv;
  
  pure.reserve(nv);
  info.reserve(nv);
  links.reserve(nv);
  open.reserve(nv);
  closed.reserve(tableSize);

  for (size_t i=0; i<tableSize; i++)
  {
    closed.push_back( std::vector<pnode>() );
    
    if (i < nv) {
      pure.push_back( state_s(i+1, LIT_UNSET) );
      info.push_back( var_info_s() );
      links.push_back( std::vector<pclause>() );
    }
  }
}

std::string Stochastic::solution()
{
  if (!soln) return "c no solution found";
  if (gui) return soln->toGUIString();
  return "c solution found: " + soln->toString();
}

std::string Stochastic::stats()
{
  float failRate = 0;
  if (toGenerate && expanded)
    failRate = 1.0f - (generated/(expanded*toGenerate));
  
  return "c " +
  std::to_string(decisions) + " decisions, " +
  std::to_string(conflicts) + " conflicts, " +
  std::to_string(restarts) + " restarts\nc " +
  std::to_string(generated) + " nodes generated, " +
  std::to_string(expanded) + " nodes expanded\nc " +
  std::to_string(tableSize) + " = closed size, " +
  std::to_string(failRate) + " = node gen. fail rate" +
  "\nc random seed was " + std::to_string(seed) + ", " +
  std::to_string(elapsedTime) + "s elapsed";
}

/************************************
 restart - try searching again, closed list maintained
 ***********************************/
LIT_VALUE Stochastic::restart()
{
  if (gui) cout << std::to_string(restarts) + " restart\n";
  restarts++;
  
  open.clear(); // don't clear the closed list
  
  for (auto i=0; i < nv; i++)
  {
    assign(i+1, LIT_UNSET);
    
    info[i].reset();
    info[i].used = false;
    info[i].val = (LIT_VALUE)(rand()%2);
    pure[i].val = LIT_UNSET;
  }
  
  update(); // calculate clause info
  
  // capture variable "skew" ratios
  float maxRatio = 0, temp = 0; size_t maxI = 0;
  for (auto i = 0; i < nv; i++)
  {
    temp = info[i].ratioRegNeg;
    if (temp > maxRatio) { maxRatio = temp; maxI = i; }
  }
  //cout << "MAX: " << maxI << "(" << maxRatio << ")" << endl;
  
  assignAll(); // propagate random settings through clauses
  
  // prepare the root node ------>
  std::vector<state_s> state = getState();
  pnode root = pnode(new Node( this )), first;
  for (auto i = 0; i < nv; i++) root->push( state[i] );

  // add initial node to open ----->
  first = pnode( new Node(this, root) );
  first->setH();
  
  if ( goal(first) ) return LIT_TRUE;
  
  first->push( maxI+1, info[maxI].bestVal() ); // give this node a value
  first->setH();
  
  if ( goal(first) ) return LIT_TRUE;
  
  addNode( first ); // make sure it gets expanded
  
  return LIT_UNSET;
}

bool Stochastic::addClause(pclause clause)
{
  c.push_back(clause);
  
  for (auto i = 0; i < clause->size(); i++)
    links[ clause->at(i)->id-1 ].push_back(clause);
  
  return true;
}

/************************************
 solve - try to find a solution to a cnf file
 ***********************************/
bool Stochastic::solve()
{
  LIT_VALUE ret = LIT_UNSET;
  size_t catchCycle, diff, openSize = 1, loop = 0;
  startTime = NOW();
  
  while(ret == LIT_UNSET && restarts < maxRestarts)
  {
    if (restart() == LIT_TRUE) return LIT_TRUE; // setup a new search

    catchCycle = expanded; openSize = open.size();
    lastTime = NOW();
    
    loop = 0;
    while(openSize < 1000)
    {
      if ( fabsf(NOW() - lastTime) > restartTime )
        break;
      
      // the max variable controls whether we're expanding the best node
      // or the absolute worst node
      size_t minH = nc; auto minI = 0; int max = 1;//rand()%2;
      for (auto i = 0; i < openSize; i++) {
        if ( max && open[i]->h < minH ) { minH = open[i]->h; minI = i; }
        else if (!max && open[i]->h > minH ) { minH = open[i]->h; minI = i; }
      }
      
#ifdef VERBOSE_MODE
      if (!gui)
      {
        printCount++;
        if (printCount % PRINT_COUNT == 0)
        {
          printCount = 0;
          
          cout << "\r\033[Kc " <<
          "open: " << openSize << ", h(n): " << minH <<
          ", nodes: " << generated << ", restarts: " << restarts;
          cout.flush();
        }
      }
#endif
      
      openSize--; diff = decisions;
      
      pnode n = open[minI];
      open.erase(open.begin()+minI, open.begin()+minI+1);
      ret = expand(n);
      if (ret == LIT_TRUE) break;
      
      openSize += decisions - diff;
    }
    
  }
  
  elapsedTime = fabsf( NOW() - startTime ); restarts--;
  return (ret == LIT_UNSET) ? LIT_FALSE : ret;
}

/************************************
 satisfiable - search driver, returns true if solution found
 will not search forever
 ***********************************/
bool Stochastic::satisfiable()
{
#ifdef VERBOSE_MODE
  if (!gui) cout << "c " << nv << " variables, " << nc << " clauses\n";
#endif
  if (gui) cout << nv << " " << nc << "\n";
  
  return solve();
  
  /*
  std::vector<feature_s> f = getDisjointGraphs();
  if (f.size() == 1) return solve();
  
  std::sort( f.begin(), f.end(), feature_sorter() );
  std::vector<Stochastic*> kids( f.size() );
  
  for (auto i = 0; i < f.size(); i++)
  {
    const size_t numV = f[i].v.size(), numC = f[i].c.size();
    kids.push_back(new Stochastic( numV, numC ));
    
    for ( auto j = 0; j < numC; j++)
      kids[i]->addClause( f[i].c[j] );
    for (auto j = 0; j < numV; j++)
      kids[i]->pure[j].lit = f[i].v[j];
    
    if ( !kids[i]->satisfiable() ) return false;
  }
  
  return true;*/
}

/************************************
 expand(node) - expands a node and generates children
 return LIT_TRUE if solution found, LIT_FALSE if unsat determined
 returns LIT_UNSET otherwise
 ***********************************/
LIT_VALUE Stochastic::expand(pnode n)
{
  if (gui) cout << n->toGUIString() << "\n";
  
  expanded++;
  if ( goal(n) ) return LIT_TRUE;
  
  n->assign();
  std::vector<size_t> gen;
  
  // ---------------------------------------------
  // always generate the minimum lit/var ratio node
  /*float minRatio = FLOAT_INFINITY; auto minI = 0;
  for (auto i = 0; i < nv; i++)
    if (info[i].used) continue;
    else if (info[i].ratioLitVar < minRatio) {
      minRatio = info[i].ratioLitVar; minI = i;
    }
  if (minRatio == FLOAT_INFINITY) { n->undo(); return LIT_UNSET; }
  gen.push_back(minI);*/
  // ---------------------------------------------
  
  
  // fill in the rest ------------------>
  for (auto i = 0; i < toGenerate-1; i++)
    if (info[i].used) continue;
    else gen.push_back(i);
  
  // begin generating ------------------>
  pnode newN = NULL; state_s mod;
  for (auto i=0; i < gen.size(); i++)
  {
    generated++;
    
    mod = state_s( gen[i]+1, INVERT(info[ gen[i] ].val) );
    newN = pnode( new Node(this, n, &mod) );
    newN->setH();
    
    //cout << newN->toString() << "\n";
    //cout << newN->h << " ";
    
    if ( !addNode(newN) ) { continue; }
    if ( goal(newN) ) return LIT_TRUE;
    
    decisions++;
  }
  
  return LIT_UNSET;
}

/************************************
 utility(node) - sets a nodes utility and fills in atomic & pure literals
 returns the assigned utility (infinity = goal)
 ***********************************/
size_t Stochastic::h(pnode n)
{
  size_t h = nc;

  std::vector<state_s> pure = getPure();
  //h -= 4 * pure.size(); // weight pure lits highly
  
  for (auto i=0; i<pure.size(); i++) // save into node
    n->push( pure[i] );
  
  std::vector<state_s> atom = getAtomic();
  //h += 3 * atom.size(); // weight atomics
  
  for (auto i=0; i<atom.size(); i++) // save into node
    n->push( atom[i] );
  
  n->assign();
  // values assigned ----------->
  
  LIT_VALUE v;
  for (auto i = 0; i < nc; i++)
  {
    v = c[i]->isSatisfied();
    if (!v) { conflicts++; }
    else { h--; }
  }
  
  // <---------- undo assignments
  n->undo();

  // u -= n->depth;
  return h;
}

/************************************
 goal(node) - check if a node is goal, record if yes, return yes/no
 ***********************************/
bool Stochastic::goal(pnode n)
{
  if (!n->h) { soln = n; return true; }
  return false;
}

/************************************
 addNode(node) - add a node to the closed list or fail if duplicate
 ***********************************/
bool Stochastic::addNode(pnode n)
{
  std::string str = n->toString();
  unsigned char *hash = utils::to_uchar( str );
  auto index = utils::HashTable::DJB2_hash( hash ); delete [] hash;
  index %= tableSize;
  
  if ( closed[index].size() )
  {
    for (auto i=0; i < closed[index].size(); i++)
      if (closed[index][i]->h == n->h &&
          closed[index][i]->toString() == str)
        return false;
  }
  
  open.push_back(n);
  closed[index].push_back(n);
  return true;
}

/************************************
 getHits(n, high) - return a list of indexes n items long
 if high is true, get the n highest vars by hit count
 otherwise get the n lowest vars (0 indicates no vars left unset)
 ***********************************/
std::vector<size_t> Stochastic::getHits(size_t n, bool high) const
{
  std::vector<size_t> ret, retVal;
  size_t last, index, count = 0;
  bool good;
  
  if (n > nv) n = nv;
  while (count++ < n)
  {
    index = 0; last = high ? 0 : -1;
    
    for (auto i = 0; i < nv; i++)
    {
      if (info[i].used || !info[i].hits) continue;
      
      if ( !ret.size() )
      {
        if (high && info[i].hits > last ) { last = info[i].hits; index = i; }
        else if (!high && info[i].hits < last ) { last=info[i].hits; index=i; }
      }
      else if (high && info[i].hits > last)
      {
        if ( info[i].hits > last && info[i].hits <= retVal[ret.size()-1])
        {
          good = true;
          for (auto j = 0; j < ret.size(); j++)
            if (ret[j]==i) { good=false; break; }
          if (good) { last = info[i].hits; index = i; }
        }
      }
      else if (!high && info[i].hits < last)
      {
        if ( info[i].hits < last && info[i].hits >= retVal[ret.size()-1])
        {
          good = true;
          for (auto j = 0; j < ret.size(); j++)
            if (ret[j]==i) { good=false; break; }
          if (good) { last = info[i].hits; index = i; }
        }
      }
    }
    
    if (!last) break; // no more hits available
    else if (!high && last == -1) break;
    
    ret.push_back(index);
    retVal.push_back(last);
  }
  
  return ret;
}

std::vector<state_s> Stochastic::getAtomic() const
{
  std::vector<state_s> ret;
  for (auto i = 0; i < nc; i++) {
    if ( c[i]->varsUnset() != 1 ) continue;
    plit unset = c[i]->nextUnset();
    ret.push_back(state_s( unset->id, unset->negated ? LIT_FALSE : LIT_TRUE ));
  }
  return ret;
}

std::vector<state_s> Stochastic::getPure()
{
  std::vector<state_s> ret;
  
  for (auto i = 0; i < nv; i++) pure[i].val = LIT_UNSET;
  for (auto i = 0; i < nv; i++)
  {
    if (info[i].used) continue;
    
    for (auto j = 0; j < links[i].size(); j++)
    {
      if (links[i][j]->isSatisfied() != LIT_UNSET) continue;
      plit l = links[i][j]->find(i+1); if (!l) continue;
      
      LIT_VALUE val = l->negated ? LIT_FALSE : LIT_TRUE;
      if (pure[i].val == LIT_UNSET) pure[i].val = val;
      else if (pure[i].val != val) pure[i].val = LIT_NOT_PURE;
    }
  }
  
  for (auto i = 0; i < nv; i++)
  {
    if (info[i].used) continue;
    else if (pure[i].val == LIT_TRUE || pure[i].val == LIT_FALSE)
      ret.push_back(pure[i]);
  }
  
  return ret;
}

std::vector<state_s> Stochastic::getState() const
{
  std::vector<state_s> ret;
  for (auto i = 0; i < nv; i++) {
    ret.push_back( state_s(i+1, info[i].val) );
  }
  return ret;
}

// for decomposing problem if it contains disjoint sub-graphs
feature_s Stochastic::getDisjoint(feature_s seen, std::vector<size_t> nextVisit)
{
  if ( !nextVisit.size() ) return seen;
  
  std::vector<size_t> next; bool dupe; size_t v=0;
  for (auto z=0; z < nextVisit.size(); z++) // loop through all vars needed
  {
    v = nextVisit[z]-1;
    for (auto i=0; i < links[v].size(); i++)
    {
      // ignore duplicate clauses ------------------------------
      dupe = false;
      for (auto j=0; j < seen.c.size(); j++)
        if (seen.c[j] == links[v][i]) { dupe = true; break; }
      if (dupe) continue;
      
      seen.c.push_back( links[v][i] );
      
      // ignore duplicate variables ----------------------------
      for (auto j=0; j < links[v][i]->size(); j++)
      {
        dupe = false;
        for (auto k=0; k < seen.v.size(); k++)
          if (seen.v[k] == links[v][i]->at(j)->id) { dupe=true; break; }
        if (dupe) continue;
        
        next.push_back( links[v][i]->at(j)->id );
        seen.v.push_back( next[next.size()-1] );
      }
    }
  }
  
  return getDisjoint(seen, next);
}

std::vector<size_t> Stochastic::getDisjointVars(size_t startVar)
{
  std::vector<size_t> next; next.push_back(startVar);
  return getDisjoint(feature_s(nv), next).v;
}

std::vector<pclause> Stochastic::getDisjointClauses(size_t startVar)
{
  std::vector<size_t> next; next.push_back(startVar);
  return getDisjoint(feature_s(nv), next).c;
}

std::vector<feature_s> Stochastic::getDisjointGraphs()
{
  std::vector<feature_s> ret;
  std::vector<size_t> hit, start;
  
  bool dupe;
  for (auto i=0; i < nv; i++)
  {
    std::sort( hit.begin(), hit.end() );
    
    dupe = false; start.clear();
    for (auto j=0; j<hit.size(); j++) // ignore duplicates
      if (hit[j]==(i+1)) { dupe=true; break; }
      else if (!start.size() && (j+1) != hit[j]) start.push_back(j+1);
    if (dupe) continue;
    
    if (!hit.size()) start.push_back(1);
    else if (!start.size() && hit.size() < nv) start.push_back(hit.size()+1);
    else if (!start.size()) break;
    else if (start[0]==nv) break; // all vars accounted for
    
    feature_s f = getDisjoint(feature_s(nv), start);
    // do variable renaming here:
    /*for (auto j = 0; j < f.v.size(); j++) {
      auto v = f.v[j];
      for (auto k = 0; i < f.c.size(); k++)
        
    }*/
    ret.push_back(f);
    
    std::sort( f.v.begin(), f.v.end() );
    
    // record new variables we haven't seen before
    for (auto j=0; j<f.v.size(); j++)
    {
      dupe = false;
      for (auto k=0; k<hit.size(); k++)
        if ( hit[k] == f.v[j] ) { dupe=true; break; }
        else if (hit[k] > f.v[j]) break;
      
      if (dupe) continue;
      hit.push_back( f.v[j] );
    }
  }
  
  return ret;
}

void Stochastic::assign(size_t var, LIT_VALUE val)
{
  if ( info[var-1].val == val ) return; // don't bother assigning twice
  
  //info[var-1].used = true;
  info[var-1].val = val;
  for (auto i = 0; i < links[var-1].size(); i++)
    links[var-1][i]->assign(var, val);
}

void Stochastic::assignAll()
{
  for (auto i = 0; i < nv; i++)
    for (auto j = 0; j < links[i].size(); j++)
      for (auto k = 0; k < links[i][j]->size(); k++)
      {
        size_t lit = links[i][j]->at(k)->id;
        links[i][j]->assign(lit, info[lit-1].val);
      }
}

void Stochastic::update()
{
  for (auto i=0; i<nv; i++) info[i].reset();
  
  float sum = 0; size_t size = 0;
  for (auto i=0; i<nc; i++)
  {
    if (c[i]->isSatisfied() == LIT_TRUE) continue;
    size = c[i]->size(); sum += (float)size;
    
    for (auto j=0; j < size; j++) {
      if ( c[i]->at(j)->negated ) info[ c[i]->at(j)->id-1 ].neg++;
      else info[ c[i]->at(j)->id-1 ].reg++;
    }
  }
  
  for (auto i=0; i<nv; i++)
  {
    // get var X / sumVars ratio
    info[i].hits = info[i].reg + info[i].neg;
    if (!sum) { info[i].ratioLitVar = 0; continue; }
    info[i].ratioLitVar = (float)info[i].hits / sum;
    
    // get ratio of positive / negative
    if (!info[i].ratioRegNeg && info[i].hits)
    {
      auto large = (info[i].neg > info[i].reg) ? info[i].neg : info[i].reg;
      info[i].ratioRegNeg = (float)large / (float)info[i].hits;
    }
  }
}

//==============================================================================
//  Node methods:
//==============================================================================

void Stochastic::Node::undo()
{
  for (auto i=0; i<state.size(); i++) {
    s->assign( state[i].lit, INVERT(state[i].val) );
    //s->info[ state[i].lit-1 ].used = false;
  }
}

void Stochastic::Node::assign()
{
  for (auto i=0; i < state.size(); i++) {
    s->assign( state[i].lit, state[i].val );
    //s->info[ state[i].lit-1 ].used = true;
  }
}

LIT_VALUE Stochastic::Node::isSatisfied()
{
  assign();
  LIT_VALUE ret = LIT_TRUE;
  for (auto i=0; i < s->nc; i++) {
    ret = s->c[i]->isSatisfied();
    if (ret == LIT_FALSE || ret == LIT_UNSET) break;
  }
  undo();
  return ret;
}

std::vector<state_s> Stochastic::Node::getState() const
{
  if (!parent) return state;
  std::vector<state_s> ret = parent->getState(); // first grab parent's state

  for (auto i=0; i < state.size(); i++) // now overwrite it with my info
    ret[ state[i].lit-1 ].val = state[i].val;
  
  return ret;
}

std::string Stochastic::Node::toString()
{
  std::vector<state_s> st = getState();
  std::sort( st.begin(), st.end(), state_sorter() );
  
  std::string ret = "";
  for (auto i = 0; i < st.size(); i++)
    ret += std::to_string(st[i].lit) + "(" + (!st[i].val ? "F) " : "T) ");
  
  return ret;
}

std::string Stochastic::Node::toGUIString() const
{
  std::vector<state_s> st = getState();
  std::sort( st.begin(), st.end(), state_sorter() );
  
  std::string ret = "";
  for (auto i = 0; i < st.size(); i++)
    ret += !st[i].val ? "F" : "T";
  
  return std::to_string(h) + " " + ret;
}