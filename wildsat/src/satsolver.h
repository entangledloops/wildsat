//
//  satsolver.h
//  wildsat
//
//  Created by Stephen Dunn on 4/10/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#ifndef wildsat_satsolver_h
#define wildsat_satsolver_h

//=================================

class Literal;
class Clause;

//=================================

#include <vector>
#include <memory>
#include <time.h>

#include "utils.h"
#include "hashtable.h"

//=================================

enum LIT_VALUE { LIT_FALSE=0, LIT_TRUE, LIT_NOT_PURE, LIT_PURE, LIT_UNSET };

#define INVERT(L) (LIT_VALUE)( (L%2==0) ? (LIT_VALUE)(L+1) : (LIT_VALUE)(L-1) )

typedef std::shared_ptr<Literal> plit;
typedef std::vector<plit> plitList;
typedef std::shared_ptr<Clause>  pclause;
typedef std::vector<pclause> pclauseList;

/********************************
 a literal
 *******************************/
class Literal : public utils::Hashable {
public:
  
  Literal(size_t id, bool negated = false, LIT_VALUE assignment = LIT_UNSET):
  id(id),negated(negated),assignment(assignment) {}
  
  Literal(const Literal &l) { id = l.id; negated = l.negated;
    assignment = l.assignment; }
  
  inline bool operator!=(const Literal &l) const { return !(*this==l); }
  inline bool operator==(const Literal &l) const {
    return ( (id == l.id) && (value() == l.value()) );
  }
  
  //=================
  
  LIT_VALUE value() const;
  void flip();
  void assign(LIT_VALUE assignment) { this->assignment = assignment; }
  size_t hash() const { return (negated) ? -id : id; }
  std::string toString() { return (negated ? "-" : "") + std::to_string(id); }
  
  size_t id;
  bool negated;
  LIT_VALUE assignment;
  
};

// add overloaded operators for shared_ptr calls
template <class Literal>
inline bool operator==(const plit l, const plit l2) { return l->id == l2->id; }
template <class Literal>
inline bool operator!=(const plit l, const plit l2) { return !(l==l2); }

// sort by literal id
struct literal_sorter {
  inline bool operator()(const plit l, const plit l2) const
  {
    if (l->assignment == LIT_UNSET) {
      if (l2->assignment != LIT_UNSET)
        return true;
    }
    else if (l2->assignment == LIT_UNSET)
      return false;
    
    return l->id < l2->id;
  }
};

/********************************
 a clause of literals
 *******************************/
class Clause : public utils::Hashable {
public:
  
  Clause(plitList lits):update(true),satisfied(LIT_UNSET),lits(lits)
  { nVarsUnset = lits.size(); }
  Clause(const Clause &c) { lits = c.lits; update = c.update;
    satisfied = c.satisfied; nVarsUnset = c.nVarsUnset; }
  
  inline bool operator==(const Clause &c) const {
    if (c.size() != size()) return false;
    for (size_t i = 0; i < size(); i++) if (*c.at(i) != *at(i)) return false;
    return true;
  }
  
  inline bool operator!=(const Clause &c) const { return !(*this == c); }
  inline plit operator[](const size_t i) const { return at(i); }
  
  //=================
  
  std::vector<plit>::iterator begin() { return lits.begin(); }
  std::vector<plit>::iterator end() { return lits.end(); }
  size_t size() const { return lits.size(); }
  size_t hash() const;
  size_t varsUnset() const;
  
  void assign(size_t lit, LIT_VALUE val);
  void needsUpdate() { update = true; }
  void sort() { std::sort(begin(), end(), literal_sorter()); }
  void add(plit lit) { lits.push_back(lit); update = true; }
  
  plit at(size_t i) const { return lits.at(i); }
  plit nextUnset() const {
    for (auto i=0;i<size();i++)
      if (at(i)->value()==LIT_UNSET) return at(i);
    return NULL;
  }
  plit find(size_t id) const {
    for (auto i=0;i<size();i++) if(at(i)->id==id) return at(i);
    return NULL;
  }
  
  void remove(plit l) {
    update = true;
    for (auto it=lits.begin(); it != lits.end();) {
      if (*l == **it) { it = lits.erase(it); continue; }
      ++it;
    }
  }
  
  bool contains(size_t id) const {
    for (size_t i=0; i<size(); i++) if (at(i)->id == id) return true;
    return false;
  }
  
  LIT_VALUE isSatisfied();
  
  std::string toString();
  
  
protected:
  
  size_t nVarsUnset;
  bool update;
  LIT_VALUE satisfied;
  plitList lits;
  
};

// sorter for sort by satisfied lits, then clause length
struct clause_sorter {
  inline bool operator()(const pclause c, const pclause c2) const
  {
    return c < c2;
  }
};

// add overloaded operators for shared_ptr calls
template <class Clause>
inline bool operator!=(const pclause c, const pclause c2) { return !(c==c2); }
template <class Clause>
inline bool operator==(const pclause c, const pclause c2) {
  if (c->size() != c2->size()) return false;
  for (size_t i = 0; i < c->size(); i++)
    if (*c->at(i) != *c2->at(i)) return false;
  return true;
}
template <class Clause>
inline bool operator<(const pclause c, const pclause c2) {
  size_t u1 = c->varsUnset(), u2 = c2->varsUnset();
  if (u1 == u2) { u1 = c->size(); u2 = c2->size(); }
  return u1 < u2;
}
template <class Clause>
inline bool operator>(const pclause c, const pclause c2) {
  return !(c < c2);
}

/********************************
 a generic SAT solver
 *******************************/
class SatSolver {
public:
  
  SatSolver(const size_t &nv, const size_t &nc, bool gui):nv(nv),nc(nc),gui(gui)
  { decisions = conflicts = restarts = 0; }
  ~SatSolver() {}
  
  //=================
  
  virtual bool addClause(pclause c) { return false; }
  virtual bool satisfiable() { return false; }
  virtual std::string solution() { return "override\n"; }
  virtual std::string stats() { return "override\n"; }
  
  //=================
  
  double decisions, conflicts; // various counters
  size_t restarts;
  const size_t nv, nc;  // # of clauses and literals
  bool gui;
  
};

#endif
