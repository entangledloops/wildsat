//
//  satsolver.cpp
//  wildsat
//
//  Created by Stephen Dunn on 5/4/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#include "satsolver.h"


//=================================
//        literal methods
//=================================

LIT_VALUE Literal::value() const
{
  if (assignment==LIT_UNSET) return LIT_UNSET;
  if (assignment==LIT_TRUE && !negated) return LIT_TRUE;
  if (!assignment && negated) return LIT_TRUE;
  return LIT_FALSE;
}

void Literal::flip()
{
  if (assignment==LIT_FALSE) assignment = LIT_TRUE;
  else if (assignment==LIT_TRUE) assignment = LIT_FALSE;
}

//=================================
//        clause methods
//=================================

void Clause::assign(size_t lit, LIT_VALUE val)
{
  for (auto i=0; i < lits.size(); i++)
  {
    if (lits[i]->id==lit) {
      if (lits[i]->assignment != val) {
        update = true;
        lits[i]->assign(val);
      }
      break;
    }
  }
}

/********************************
 am i satisfied?
 *******************************/
LIT_VALUE Clause::isSatisfied()
{
  if (!update) return satisfied;
  nVarsUnset = varsUnset(); // update unset variables count
  update = false;
  satisfied = LIT_FALSE;
  
  for (auto i=0;i<size();i++)
  {
    if (at(i)->value()==LIT_TRUE) {
      satisfied = LIT_TRUE;
      return satisfied;
    }
    if (at(i)->value()==LIT_UNSET) satisfied = LIT_UNSET;
  }
  
  return satisfied;
}

/********************************
 return string representation
 *******************************/
std::string Clause::toString()
{
  std::string ret = "";
  for (auto i = 0; i < lits.size(); i++)
  {
    if (i) ret += " ";
      if (lits[i]->negated) ret += "-";
        ret += std::to_string(lits[i]->id);
  }
  
  return ret;
}

/********************************
 return a hash value
 *******************************/
size_t Clause::hash() const
{
  size_t hash = size();
  for (auto i = 0; i < size(); i++) hash += at(i)->hash();
    return hash;
}

/********************************
 return # of unset vars in clause
 *******************************/
size_t Clause::varsUnset() const
{
  if (!update) return nVarsUnset;
  size_t ret = 0;
  for (auto i=0; i < lits.size(); i++)
    if (lits[i]->assignment == LIT_UNSET) ret++;
  return ret;
}
