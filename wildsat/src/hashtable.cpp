//
//  hashtable.cpp
//  wildsat
//
//  Created by Stephen Dunn on 4/12/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#include "hashtable.h"

using std::cout; using std::endl;

/********************************
 add new key/value pair to the hashtable
 *******************************/
void utils::HashTable::add(std::string id, Hashable *val)
{
  addWithHash( hash(id), id, val );
}

void utils::HashTable::addWithHash(size_t hash, std::string id, Hashable *val)
{
  size_t index = hash % size();
  pnode temp =  table[index];
  pnode newNode = pnode( new Node(id, val) );
  
  if (!temp) { table[index] = newNode; return; }
  while (temp->next) temp = temp->next;
  temp->next = newNode;
}

utils::Hashable *utils::HashTable::get(std::string &id)
{
  size_t index = hash(id)%size();
  pnode temp = table[index];
  while (temp) {
    if (temp->id == id) return temp->val;
    temp=temp->next;
  }
  return NULL;
}

/********************************
 hash a key using chosen hash method
 *******************************/
size_t utils::HashTable::hash(unsigned char *s)
{
  if (hasher==DJB2_HASH) return DJB2_hash(s);
  return NULL;
}

size_t utils::HashTable::hash(std::string &s)
{
  return hash( utils::to_uchar(s) );
}

/********************************
 different hash methods to choose from
 *******************************/

// hash algorithm adapted from Dan Bernstein (U. Illinois)
size_t utils::HashTable::DJB2_hash(unsigned char *s)
{
  size_t hash = 5381; int c;
  while ( (c = *s++) ) hash = ((hash << 5) + hash) + c;
  return hash;
}