//
//  utils.h
//  wildsat
//
//  Created by Stephen Dunn on 3/14/13.
//
//

#ifndef __wildsat__utils__
#define __wildsat__utils__

#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>
#include <sstream>
#include <limits>
#include <memory>
#include <math.h>

namespace utils {
  
  typedef std::vector<std::string> svec;
  typedef std::vector<std::string*> spvec;
  typedef std::vector<uint> uivec;
  typedef std::vector<float> fvec;
  typedef std::deque<float> fdque;
  
  /********************************
   string and vector manipulation
   ********************************/
  
  // helper for cleaning up vector * of T*
  struct del_ptr {
    template <typename T>
    void operator() (const T *p) { if (p) delete p; p = NULL; }
  };
  
  // helper for cleaning up array of T*
  struct del_array {
    template <typename T>
    void operator() (const T *p) { if (p) delete [] p; p = NULL; }
  };
  
  template <typename T> void free(std::vector<T*, std::allocator<T*>> *v) {
    if (v) {
      std::for_each(v->begin(), v->end(), del_ptr());
      delete v; v = NULL;
    }
  }
  
  template <typename T> void free(std::vector<T*, std::allocator<T*>> &v) {
    std::for_each(v.begin(), v.end(), del_ptr());
    v.clear();
  }
  
  // deep copy vector of T*
  template <typename T>
  std::vector<T*> *copy(const std::vector<T*, std::allocator<T*>> &v)
  {
    std::vector<T*> *ret = new std::vector<T*>();
    for (int i = 0; i < v.size(); i++)
      ret->push_back( new T( *(v[i]) ) );
    return ret;
  }
  
  // merge 2 into 1 (1 <- 2), deletes 2
  template <typename T>
  std::vector<T*, std::allocator<T*> > *merge(std::vector<T*,
                                              std::allocator<T*>> *v,
                                              std::vector<T*,
                                              std::allocator<T*>> *v2)
  {
    if (!v || !v2) return v;
    while ( !v2->empty() ) { v->push_back(v2->back()); v2->pop_back(); }
    utils::free(v2);
    return v;
  }
  
  // merge 2 into 1 (1 <- 2) no duplicates, deletes 2
  template <typename T>
  std::vector<T*, std::allocator<T*> > *mergeUnique(std::vector<T*,
                                                    std::allocator<T*> > *v,
                                                    std::vector<T*,
                                                    std::allocator<T*> > *v2)
  {
    if (!v || !v2) return v;
    
    while ( !v2->empty() ) {
      T *t = v2->back(); v2->pop_back();
      for (int i = 0; i < v->size(); i++)
        if (*v->at(i) == *t) { delete t; t = NULL; break; }
      if (t) v->push_back(t);
    }
    
    utils::free(v2);
    return v;
  }
  
  unsigned char *to_uchar(std::string s);
  std::istream &readline(std::string &s);
  std::shared_ptr<fvec> getFloatsVector(std::shared_ptr<svec> v);
  std::shared_ptr<fdque> getFloatsDeque(std::shared_ptr<svec> v);
  std::string &trim(std::string &s, char delim);
  std::string &trim(std::string &s);
  svec *split(std::string &str, char delim);
  svec &split(svec &list, std::string &str, char delim);
  
  /********************************
   math
   ********************************/
  
  float avg(std::shared_ptr<fvec> v);
  float var(std::shared_ptr<fvec> v);
  float stdev(std::shared_ptr<fvec> v);
  float r(std::shared_ptr<fvec> x, std::shared_ptr<fvec> y);
  float r(std::shared_ptr<fvec> x, std::shared_ptr<fvec> y,
          float avgX, float avgY, float stdX, float stdY);
  
}

#endif /* defined(__wildsat__utils__) */
