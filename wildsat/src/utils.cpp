//
//  utils.cpp
//  General C++ utilities library to complement the STL
//
//  Created by Stephen Dunn on 3/14/13.
//
//

#include "utils.h"

/********************************
 utilities to compensate for missing STL features
 *******************************/

namespace utils {
  
  using std::string; using std::vector; using std::cin;
  using std::istream; using std::stringstream;
  
  /********************************
   string and vector manipulation
   ********************************/
  
  // convert an STL string -> non-const char*
  unsigned char *to_uchar(std::string s) {
    unsigned char *temp = new unsigned char[s.size()+1];
    std::copy( s.begin(), s.end(), temp );
    temp[ s.size() ] = '\0';
    return temp;
  }
  
  // read a line skipping junk
  istream &readline(std::string &s) {
    s = "";
    while (!s.length()) if (!getline(cin, s)) break;
    return cin;
  }
  
  std::shared_ptr<fvec> getFloatsVector(std::shared_ptr<svec> v) {
    std::shared_ptr<fvec> ret ( new fvec() ); float j;
    for (size_t i = 0; i < v->size(); i++, ret->push_back(j))
      std::stringstream(v->at(i)) >> j;
    return ret;
  }
  
  std::shared_ptr<fdque> getFloatsDeque(std::shared_ptr<svec> v) {
    std::shared_ptr<fdque> ret ( new fdque() ); float j;
    for (size_t i = 0; i < v->size(); i++, ret->push_back(j))
      std::stringstream(v->at(i)) >> j;
    return ret;
  }
  
  // trim
  std::string &trim(std::string &s, char delim) {
    if (s.length() <= 0) { s = ""; return s; }
    size_t len = s.length(), start = 0, size = len;
    
    for (size_t i = 0; i < len; i++)
      if ( s[i] != delim && s[i] != '\n' && s[i] != '\t' ) {
        start = i;
        break;
      }
    
    for (size_t i = len-1, count = 0; i >= start; i--, count++)
      if ( s[i] != delim && s[i] != '\n' && s[i] != '\t') {
        size = (len-start) - count;
        break;
      }
    
    if ( !size || start+1 > s.length() ) s = "";
    else s = s.substr(start, size);
    
    return s;
  }
  
  // trim wrapper for usual ' ' trimming
  std::string &trim(std::string &s) { return trim(s, ' '); }
  
  // split
  svec *split(std::string &str, char delim) {
    svec *list = new svec();
    return &split(*list, str, delim);
  }
  
  // split
  svec &split(svec &list, std::string &str, char delim) {
    stringstream ss(str);
    string cur;
    
    list.clear();
    while ( getline(ss, cur, delim) )
      if (trim(cur, delim).length() != 0)
        list.push_back( cur );
    
    return list;
  }
  
  /********************************
   math
   ********************************/
  
  float avg(std::shared_ptr<fvec> v)
  {
    if (!v->size()) return 0;
    float avg = 0;
    for (size_t i = 0; i < v->size(); i++) avg += v->at(i);
    return avg/(float)v->size();
  }
  
  float var(std::shared_ptr<fvec> v)
  {
    if (!v->size()) return 0;
    float var = 0;
    for (size_t i = 0; i < v->size(); i++) var += (v->at(i)*v->at(i));
    return var/(float)v->size();
  }
  
  float stdev(std::shared_ptr<fvec> v) { return sqrt( var(v) ); }
  
  float r(std::shared_ptr<fvec> x, std::shared_ptr<fvec> y)
  {
    float avgX = avg(x), avgY = avg(y);
    float stdX = stdev(x), stdY = stdev(y);
    return r(x, y, avgX, avgY, stdX, stdY);
  }
  
  // calculate relationship r between set x and set y
  float r(std::shared_ptr<fvec> x, std::shared_ptr<fvec> y,
          float avgX, float avgY, float stdX, float stdY)
  {
    if (x->size() != y->size()) return 0;
    float xN = 0, yN = 0, r = 0;
    for (size_t i = 0; i < x->size(); i++) {
      xN = (x->at(i) - avgX) / stdX;
      yN = (y->at(i) - avgY) / stdY;
      r += xN * yN;
    }
    return r/(float)x->size();
  }
  
}
