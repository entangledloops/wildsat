//
//  wildsat.h
//  
//
//  Created by Stephen Dunn on 4/10/13.
//
//

#ifndef ____wildSAT__
#define ____wildSAT__

//=================================

#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <memory>

#include "utils.h"
#include "satsolver.h"
#include "dpll.h"
#include "stochastic.h"

//=================================

#define ALG_DPLL 0
#define ALG_HILL 1
#define ALG_WILD 2

//=================================

class WildSAT {
public:
  
  WildSAT(uint algorithm, std::string filename, bool gui);
  ~WildSAT() { if (in.is_open()) in.close(); }
  
  bool satisfiable();
  std::string solution() { return algorithm->solution(); }
  std::string stats() { return algorithm->stats(); }
  
protected:
  
  bool gui;
  size_t nv, nc; // # of clauses, # of literals
  std::ifstream in;
  std::unique_ptr<SatSolver> algorithm;
  
};

#endif /* defined(____wildSAT__) */
