#!/bin/bash

for i in {1..50}
do
  for j in 100 #100 125 150 175 200 225 250
  do
    echo "hill ${j} ${i}"
    echo "$(timeout 10 ./wildsat hill cnf/${j}/uf${j}-0${i}.cnf)"
    echo ""
    echo "dpll ${j} ${i}"
    echo "$(timeout 10 ./wildsat dpll cnf/${j}/uf${j}-0${i}.cnf)"
    echo ""
  done
done
