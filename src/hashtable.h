//
//  hashtable.h
//  wildsat
//
//  Created by Stephen Dunn on 4/12/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#ifndef __wildsat__hashtable__
#define __wildsat__hashtable__

#include <iostream>
#include <cstring>

#include "utils.h"

namespace utils {
  
  enum HASH_FUNCTION { DJB2_HASH=0 };
  
  class Hashable {
  public:
    virtual std::string toString() = 0;
  };
  
  class HashTable {
  public:
    
    HashTable(HASH_FUNCTION hasher=DJB2_HASH):hasher(hasher),tableSize(0) {}
    HashTable(const size_t &size, HASH_FUNCTION hasher=DJB2_HASH)
    {
      tableSize = size;
      table.reserve(size);
      this->hasher = hasher;
    }
    ~HashTable() {}
    
    inline Hashable *operator [] (std::string &id) { return get(id); }
    
    void add(Hashable *val) { add( val->toString(), val ); }
    void add(std::string id, Hashable *val);
    void addWithHash(size_t hash, std::string id, Hashable *val);
    void reserve(size_t s) { tableSize = s; table.reserve(s); }
    
    Hashable *get(std::string &id);
    
    size_t hash(unsigned char *s);
    size_t hash(std::string &s);
    size_t size() { return tableSize; }
    
    // hash implementations:
    static size_t DJB2_hash(unsigned char *s);
    
    // ========================================
    
  protected:
    
    class Node;
    typedef std::shared_ptr<HashTable::Node> pnode;
    
    class Node : public std::enable_shared_from_this<HashTable::Node> {
    public:
      
      Node(std::string id, Hashable *val):id(id),val(val),next(NULL) {}
      
      pnode getThis() { return shared_from_this(); }
      
      std::string id;
      Hashable *val;
      pnode next;
      
    };
    
    // ========================================
    
    HASH_FUNCTION hasher;
    size_t tableSize;
    std::vector<pnode> table;
    //static size_t (HashTable<T>::*hashFunction)(const unsigned char *);
    
  };

}

#endif /* defined(__wildsat__hashtable__) */
