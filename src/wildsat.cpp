//
//  wildsat.cpp
//  
//
//  Created by Stephen Dunn on 4/10/13.
//
//

#include "wildsat.h"

using std::cout; using std::cerr; using std::endl; using std::ifstream;

/********************************
 prepare to parse clauses
 *******************************/
WildSAT::WildSAT(uint algorithm, std::string filename, bool gui)
{
  this->gui = gui;
  in.open(filename, std::ios::in);
  if (!in.good()) { cerr << "bad cnf file" << endl; throw; }
  
  // read through comment lines
  std::string line;
  while ( in.good() )
  {
    getline(in, line);
    line = utils::trim(line);
    if (line.at(0) == 'c') continue;
    break;
  }
  if (line.at(0) != 'p') { cerr << "bad cnf file" << endl; throw; }
  
  // parse the brief header
  utils::svec *s = utils::split( line, ' ' );
  std::stringstream( s->at(2) ) >> nv; // # of variables
  std::stringstream( s->at(3) ) >> nc; // # of clauses
  delete s;
  
  if (algorithm == ALG_DPLL)
    this->algorithm = std::unique_ptr<SatSolver>( new DPLL(nv, nc, gui) );
  else if (algorithm == ALG_HILL)
    this->algorithm = std::unique_ptr<SatSolver>( new Stochastic(nv, nc, gui) );
  else { cerr << "no such algorithm" << endl; throw; }
}

/********************************
 read clauses and begin solving
 *******************************/
bool WildSAT::satisfiable()
{
  plitList lits; bool negated;
  utils::svec litStrings;
  std::string line;
  bool eoc = true; // end of clause (EOC) marker '0'

  // parse all clauses from cnf file and feed to the solver
  while ( in.good() )
  {
    if (!getline(in, line)) break;
    litStrings = utils::split(litStrings, line, ' ');
    if (eoc) { lits.clear(); eoc = false; }
    
    size_t id;
    for (auto i = 0; i < litStrings.size(); i++, negated = false)
    {
      if (litStrings[i].at(0) == '-')
      {
        negated = true;
        litStrings[i].erase( litStrings[i].begin() );
      }

      std::stringstream(litStrings[i]) >> id;
      if (id == 0) { eoc = true; break; } // test for EOC marker
      
      // check for duplicates
      for (auto i=0; i<lits.size(); i++)
        if ( lits[i]->id==id && lits[i]->negated==negated ) continue;

      lits.push_back( plit(new Literal(id, negated)) );
    }
    
    if (eoc) // clause is complete, add it (false if implementation says so)
      if (!algorithm->addClause(pclause( new Clause(lits) )))
        return false;
  }
  
  return algorithm->satisfiable();
}