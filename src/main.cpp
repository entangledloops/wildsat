//
//  main.cpp
//  wildsat
//
//  Created by Stephen Dunn on 4/10/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#include "main.h"

using std::cout; using std::cerr; using std::endl; using std::string;

int main(int argv, char **argc)
{
  if (argv < 3) { cerr << "missing argument" << endl; return 1; }
  uint algorithm = 0; bool gui = false; short alg = 1;
  
  if (!strcmp(argc[1],"gui")) { gui = true; alg++; }
  if (gui && argv < 4) { cerr << "missing argument" << endl; return 2; }
  if      (!strcmp(argc[alg], "dpll")) algorithm = ALG_DPLL;
  else if (!strcmp(argc[alg], "hill")) algorithm = ALG_HILL;
  else if (!strcmp(argc[alg], "wild")) algorithm = ALG_WILD;
  else    { cerr << "unknown algorithm" << endl; return 3; }
  
  string filename  = argc[alg+1];
  std::unique_ptr<WildSAT> wildsat ( new WildSAT(algorithm, filename, gui) );
  
  if (!gui) cout << "c WildSAT v0.1\n" << "c solving: " << filename << "\n";
  if ( wildsat->satisfiable() ) // run solver
  {
    cout << "s SATISFIABLE" << endl;
    cout << wildsat->solution() << "\n";
  }
  else cout << "s UNSATISFIABLE" << endl;
  cout << wildsat->stats() << endl;
  
  return 0;
}