//
//  dpll.cpp
//  wildsat
//
//  Created by Stephen Dunn on 4/10/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#include "dpll.h"
using std::cout; using std::endl;

//=================================
// DPLL methods:

/********************************
 constructor
 *******************************/
DPLL::DPLL(const size_t &nv, const size_t &nc, bool gui) : SatSolver(nv,nc,gui)
{
  root = pnode( new Node(this) );
  pureLits = sortCount = printCount = 0;
  cv = 1;
  
  taken = new bool[nv];
  std::fill_n(taken, nv, false);
  
  c.reserve(nc);
  links.reserve(nv);
  pure.reserve(nv);
  
  for (auto i = 1; i < nv+1; i++) {
    links.push_back( std::vector<pclause>() );
    pure.push_back( pure_lit(i) );
  }
}

DPLL::~DPLL()
{
  if (taken) { delete [] taken; taken = NULL; }
}

/********************************
 prune clauses with pure literals
 *******************************/
bool DPLL::addClause(pclause clause)
{
  clause->sort(); // sort literals low->high
  c.push_back(clause);
  
  // record all clauses this literal appears in
  for (auto i = 0; i < clause->size(); i++)
    links[ clause->at(i)->id-1 ].push_back(clause);
  
  // record purity - moved into Node
  /*
   for (auto i = 0; i < clause->size(); i++)
   {
   auto id = clause->at(i)->id - 1;
   if (pure[id].val == LIT_NOT_PURE) continue;
   
   LIT_VALUE val = clause->at(i)->negated ? LIT_FALSE : LIT_TRUE;
   if (pure[id].val == LIT_UNSET) pure[id].val = val;
   else if (pure[id].val != val) pure[id].val = LIT_NOT_PURE;
   }*/
  
  return true;
}

/********************************
 satisfiable()
 determine satisfiability by DPLL
 *******************************/
bool DPLL::satisfiable()
{
  LIT_VALUE ret = LIT_UNSET;
  
#ifdef VERBOSE_MODE
  if (!gui) cout << "c " << nv << " variables, " << nc << " clauses\n";
#endif
  if (gui) cout << nv << " " << nc << "\n";
  
  // START -------------------------
  startTime = lastTime = NOW();
  ret = root->add();
  elapsedTime = fabsf(NOW() - startTime);
  // END ---------------------------
  
  return (ret == LIT_TRUE) ? true : false;
}

std::string DPLL::stats()
{
  return "c " +
  std::to_string(pureLits) + " pure literals, " +
  std::to_string(decisions) + " decisions, " +
  std::to_string(conflicts) + " conflicts, " +
  std::to_string(restarts) + " restarts\nc " +
  std::to_string(elapsedTime) + " seconds elapsed";
}

std::string DPLL::solution()
{
  return root->next->toString();
}


/********************************
 nextVar()
 returns next var to set, atomics first
 *******************************/
plit DPLL::nextVar()
{
  plit ret = NULL; size_t count = 0;
  
  for (auto i=0; i < nc; i++) // find the next unset variable
  {
    if (c[i]->isSatisfied() != LIT_UNSET) continue;
    size_t temp = c[i]->varsUnset();
    if (temp <= count) continue;
    
    for (auto j = 0; j < c[i]->size(); j++)
    {
      plit lit = c[i]->at(j);
      if (lit->assignment == LIT_UNSET && !taken[ lit->id-1 ]) {
        count = temp;
        ret = lit;
        if (count == 1) return ret; // always choose atomic immediately
      }
    }
  }
  
  return ret;
}

//==============================================================================
//  Node methods:
//==============================================================================

/********************************
 undo()
 undo this nodes assignments
 *******************************/
void DPLL::Node::undo()
{
  dpll->cv -= lits.size();
  dpll->conflicts++;
  
  for (auto i = 0; i < lits.size(); i++) {
    auto id = lits[i]-1;
    
    dpll->taken[id] = false; // free this var
    
    // undo all linked clauses
    for (auto j = 0; j < dpll->links[id].size(); j++)
      dpll->links[id][j]->assign(id+1, LIT_UNSET);
    
    // undo pure literals
    for (auto j = 0; j < dpll->nv; j++)
      if (dpll->pure[j].id == id+1) {
        dpll->pure[j].val = LIT_UNSET;
        break;
      }
  }
}

/********************************
 assign()
 assign values to literals correctly
 *******************************/
void DPLL::Node::assign()
{
  for (auto i = 0; i < lits.size(); i++)
  {
    auto id = lits[i]-1;
    for (auto j = 0; j < dpll->links[id].size(); j++)
      dpll->links[id][j]->assign(id+1, vals[i]);
  }
}

/********************************
 push(literal id, value)
 assign a literal to this node
 *******************************/
void DPLL::Node::push(size_t l, LIT_VALUE v)
{
  if (dpll->taken[l-1]) return;
  dpll->taken[l-1] = true;
  dpll->decisions++;
  dpll->cv++;
  vals.push_back(v); lits.push_back(l);
}

/********************************
 add(node, value)
 add a new literal recursively
 *******************************/
LIT_VALUE DPLL::Node::add(LIT_VALUE val)
{
  LIT_VALUE ret = LIT_UNSET;
  next = pnode(new Node( dpll ));
  next->setPureLiterals();
  
#ifndef DEBUG_MODE
#ifdef VERBOSE_MODE
  if (!dpll->gui)
  {
    dpll->printCount++;
    if (dpll->printCount % PRINT_NODE_COUNT == 0)
    {
      dpll->printCount = 0;
      cout << "\r\033[K" << "c " <<
      dpll->decisions << " decisions, " <<
      dpll->restarts << " restarts";
      cout.flush();
    }
  }
#endif
#endif
  
  /*
   // if generation took too long, sort clauses by unset variables
   dpll->sortCount++;
   if ( dpll->sortCount % SORT_NODE_COUNT == 0)
   {
   dpll->sortCount = 0;
   float diff = fabs(((float)clock()/CLOCKS_PER_SEC)-dpll->lastTime);
   if (diff > SORT_NODE_TIME)
   {
   //std::sort( dpll->c.begin(), dpll->c.end(), clause_sorter() );
   dpll->lastTime = (float)clock()/CLOCKS_PER_SEC;
   dpll->elapsedTime += diff;
   }
   }*/
  
  plit nextUnsetVar = dpll->nextVar();
  if (!nextUnsetVar && !next->lits.size()) return isSatisfied();
  if (nextUnsetVar) next->push( nextUnsetVar->id,  val );
  
#ifdef DEBUG_MODE
  // dpll->cv-1 = the # of variables set so far (including this node)
  if (!dpll->gui)
    cout << dpll->cv-1 << ": " << dpll->root->next->toString() << "\n";
#endif
  
  if (dpll->gui)
    cout << std::to_string(dpll->cv) << " " <<
    dpll->root->next->toGUIString() << "\n";
  
  // bail early if possible:
  ret = next->isSatisfied();
  if (ret == LIT_TRUE) return LIT_TRUE; // solution found!
  else if (ret == LIT_FALSE)
  {
#ifdef DEBUG_MODE
    if (nextUnsetVar && !gui) cout << "conflict @" << nextUnsetVar->id << endl;
#endif
    next->undo(); next = NULL; return ret;
  }
  
  // recurse
  return next->add();
}

/********************************
 add()
 add a new literal recursively - depth first (helper)
 *******************************/
LIT_VALUE DPLL::Node::add()
{
  if (dpll->cv > dpll->nv) return isSatisfied();
  
  //while (skipCount < dpll->nv)
  //{
  if ( add(LIT_TRUE) == LIT_TRUE ) return LIT_TRUE;
  if (next) { next->undo(); next = NULL; }
  if ( add(LIT_FALSE) == LIT_TRUE ) return LIT_TRUE;
  if (next) { next->undo(); next = NULL; }
  skipCount++; dpll->restarts++;
  //}
  
  return LIT_FALSE;
}


/********************************
 prune clauses with pure literals
 *******************************/
void DPLL::Node::setPureLiterals()
{
  bool pureFound = true;
  plit l = NULL;
  size_t id = 0, i = 0, j = 0;
  LIT_VALUE val = LIT_UNSET;
  
  while (pureFound)
  {
    pureFound = false; // bail if we don't find any pure lits
    
    // prepare for next loop (vars may have become pure since setting a var)
    for (i=0; i < dpll->nv; i++)
    {
      if ( !dpll->taken[i] && !dpll->pure[i].pure() )
        dpll->pure[i].val = LIT_UNSET;
    }
    
    // find all pure lits and mark them
    for (i=0; i < dpll->nc; i++) // for each clause
    {
      if (dpll->c[i]->isSatisfied() != LIT_UNSET) continue; // ignore set clause
      
      for (j=0; j < dpll->c[i]->size(); j++) // for each literal in this clause
      {
        l = dpll->c[i]->at(j);
        id = l->id;
        val = l->negated ? LIT_FALSE : LIT_TRUE;
        
        //if (id==1)
        // cout << "id(" << dpll->pure[id-1].id << ") val(" <<
        // dpll->pure[id-1].val << ")" << endl;
        
        if ( dpll->taken[id-1] || dpll->pure[id-1].pure() ||
            dpll->pure[id-1].val == LIT_NOT_PURE)
          continue;
        
        //if (id==1)
        //   cout << "val(" << val << ") <--> pure(" <<
        //  dpll->pure[id-1].val << ")" << endl;
        
        if (dpll->pure[id-1].val == LIT_UNSET)
          dpll->pure[id-1].val = val;
        else if (dpll->pure[id-1].val != val)
          dpll->pure[id-1].val = LIT_NOT_PURE;
        
        // if (id==1)
        //  cout << "val(" << val << ") <--> pure(" <<
        //  dpll->pure[id-1].val << ")" << endl;
      }
      
    }
    
    //std::sort( dpll->pure.begin(), dpll->pure.end(), pure_sorter() );
    
    // save pures in this node
    for (i = 0; i < dpll->nv; i++)
    {
      if (dpll->taken[ dpll->pure[i].id-1 ]) continue; // ignore used vars
      
      if ( dpll->pure[i].val != LIT_UNSET // this var must still be around
          && dpll->pure[i].val != LIT_NOT_PURE // ignore non-pures
          && dpll->pure[i].val != LIT_PURE ) // don't bother if already pure
      {
#ifdef DEBUG_MODE
        if (!gui) cout << "pure found: " << dpll->pure[i].id << endl;
#endif
        
        push( dpll->pure[i].id, dpll->pure[i].val ); // save pure
        assign(); // assign to clauses
        
        dpll->pure[i].val = LIT_PURE; // ensure we don't revisit it
        dpll->pureLits++; // stats
        
        pureFound = true; // set so we recheck for more
        break; // move on to next iteration of main loop
      }
    }
    
  }
  
  dpll->decisions -= lits.size(); // don't count pure lits
}

/********************************
 isSatisfied()
 anything other than T on everyone and we aren't fully satisfied yet
 *******************************/
LIT_VALUE DPLL::Node::isSatisfied()
{
  LIT_VALUE ret = LIT_UNSET;
  assign(); // set my literal values in dpll
  
  for (auto i = 0; i < dpll->nc; i++)
  {
    if ( !(ret = dpll->c[i]->isSatisfied()) ) return LIT_FALSE;
    else if (ret == LIT_UNSET) return LIT_UNSET;
  }
  
  return LIT_TRUE; // if we get here, must be a solution
}

/********************************
 get the full node string
 *******************************/
std::string DPLL::Node::toString()
{
  std::string ret = "";
  
  for (auto i = 0; i < vals.size(); i++)
  {
    ret += i ? " " : "";
    ret += std::to_string(lits[i]) + "(";
    if (!vals[i]) ret += "F)";
    else if (vals[i] == LIT_TRUE) ret += "T)";
    else if (vals[i] == LIT_UNSET) ret += "U)";
  }
  
  return (next) ? (ret + " " + next->toString()) : ret;
}

std::string DPLL::Node::toGUIString()
{
  std::string ret = "";
  
  for (auto i = 0; i < vals.size(); i++)
  {
    if (!vals[i]) ret += "F";
    else if (vals[i] == LIT_TRUE) ret += "T";
    else if (vals[i] == LIT_UNSET) ret += "U";
  }
  
  return (next) ? (ret + next->toGUIString()) : ret;
}