//
//  dpll.h
//  wildsat
//
//  Created by Stephen Dunn on 4/10/13.
//  Copyright (c) 2013 Stephen Dunn. All rights reserved.
//

#ifndef __wildsat__dpll__
#define __wildsat__dpll__

//=================================

class Node;
class DPLL;

//=================================

#include <iostream>
#include <time.h>

#include "satsolver.h"

//#define DEBUG_MODE // debug output (print full node strings)
//#define VERBOSE_MODE // render node counts during search & other stats
#define RENDER_SOLUTION // display a solution if satisfiable?
#define PRINT_NODE_COUNT 10000 // print current node count every X nodes
#define SORT_NODE_COUNT 1000 // check time every X nodes
#define SORT_NODE_TIME 0.2f // sort if elapsed > X (seconds)
#define FLOAT_INFINITY std::numeric_limits<float>::infinity()
#define NOW() (float)clock()/CLOCKS_PER_SEC

//=================================

class DPLL;

struct pure_lit
{
  pure_lit(size_t id) { val = LIT_UNSET; this->id = id; }
  bool pure() { return (val == LIT_PURE); }
  bool purePending() { return (val != LIT_UNSET && val != LIT_NOT_PURE); }
  LIT_VALUE val;
  size_t id;
};

struct pure_sorter
{
  inline bool operator()(pure_lit l1, pure_lit l2) const
  {
    if (l1.purePending()) // l1 is pure
      if (l2.purePending()) return l1.id < l2.id;
      else return true;
    else if (l2.purePending()) return false;// l1 isn't, l2 is
      
    return l1.id < l2.id; // both aren't
  }
};


//=================================

class DPLL : public SatSolver {
  
public:
  
  class Node;
  typedef std::shared_ptr<DPLL::Node> pnode;
  
  DPLL(const size_t &nc, const size_t &nv, bool gui);
  ~DPLL();

  virtual bool addClause(pclause c);
  virtual bool satisfiable();
  virtual std::string solution();
  virtual std::string stats();
  
  
  plit nextAtomic();
  plit nextVar();
  
  // ==============================
  bool *taken;
  double pureLits;
  float startTime, elapsedTime, lastTime;
  
  size_t cv, sortCount, printCount; // current var and count-helpers
  std::vector<pure_lit> pure;
  std::vector<std::vector<pclause>> links;
  pclauseList c;
  pnode root;
  
  
// ================================
  
  class Node : public std::enable_shared_from_this<DPLL::Node> {
  public:
    
    Node(DPLL *dpll):dpll(dpll),skipCount(0),next(NULL) {}
    ~Node() {}
    
    pnode getThis() { return shared_from_this(); }
    
    void undo();
    void assign();
    void setPureLiterals();
    void push(size_t l, LIT_VALUE v);
    
    LIT_VALUE add(LIT_VALUE val);
    LIT_VALUE add();
    LIT_VALUE isSatisfied();
    
    std::string toString();
    std::string toGUIString();

    // ====================
    
    DPLL *dpll;
    size_t skipCount;
    std::vector<size_t> lits; // literal ids set for this node
    std::vector<LIT_VALUE> vals; // values in this model
    pnode next;
    
  };
  
};

#endif /* defined(__wildsat__dpll__) */
